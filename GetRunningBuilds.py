from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script listing executed builds and ons in the queueat this moment  in a Jenkins url.           ##
#   This function should provide node health information as well soon.                                          ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GetRunningBuilds.py 'http://jenkyriba.kod.kyriba.com/' \                                             ##
#                           --nameSize 50                                                                       ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for build status distribution.",
                                 epilog="Sample: python GrepBuildStatusDistribution.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--nameSize", type=int, nargs='?', help="size of considered name when grouping results", default=20)
args = parser.parse_args()

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', None)

def getExecutableUrls(executor):
    result = []
    if ('currentExecutable' in executor) and executor['currentExecutable']:
        executable = executor['currentExecutable']
        if 'url' in executable:
            if "KApp_pull_request_pipeline/job/PR-2900/1/" in executable['url']:
                print (executor)
            build = commonfiles.removeHeaderUrl(executable['url'])
            result.append(build)
    return result


# Retrieve list of considered builds
for url in args.jenkinsUrl:
    url = commonfiles.getRootUrl(url)
    running = commonBuilds.getJenkinsActivity(url)
    pending= commonBuilds.getPendingBuilds(url)

    # Display number of in progress builds
    builds = []
    groups = {}
    numComputers = 0
    numBuilds = 0
    for computer in running['computer']:
        numComputers = numComputers + 1

        if 'assignedLabels' in computer:
            labels = computer["assignedLabels"]
            if labels:
                label = labels[0]["name"].lower()
                if not label in groups:
                    groups[label] = 0

                if 'executors' in computer:
                    for exec in computer['executors']:
                        builds.append(getExecutableUrls(exec))
                        groups[label] = groups[label] + 1
                        numBuilds = numBuilds + 1

                if 'oneOffExecutors' in computer:
                    for exec in computer['oneOffExecutors']:
                        builds.append(getExecutableUrls(exec))
                        groups[label] = groups[label] + 1
                        numBuilds = numBuilds + 1


    print ("Number of running builds", str(numBuilds).ljust(100))
    print ("Number of used Nodes", str(numComputers).ljust(100))
    print ("Number of pending builds", str(len(pending["items"])).ljust(100))
    print ("Number of nodes per group:")
    for key, value in groups.items():
        print ("- ", key.ljust(50), ": ", value)

    df = pd.DataFrame(builds, columns=['name'])
    df['name'] = df['name'].str[:args.nameSize]
    buildCounter = df.groupby('name').size()
    print ("Most frequent running builds:")
    for name, size in buildCounter.sort_values(ascending=False).head(10).items():
        print("- ", name.ljust(50), ': ', size)
