from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import re
import common.files as commonfiles
import common.plot as commonPlot
import common.colors as commonColors
from collections import Counter

##################################################################################################################
##  Parametrized script searching for downloaded images in a set of builds.                                     ##
#   This script relies on standard pattern we use to find when downloading a docker image.builds                ##
#   containing this keyword.                                                                                    ##
#   Sample:                                                                                                     ##
#   python GrepImagesFromLogs.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################


parser = argparse.ArgumentParser(description="Search for string usage in every underlying Jenkins build.\
                                              By default searched string is 'docker.io' and considered builds are \
                                              from one month only.",
                                 epilog="Sample: python GrepStringFromLog.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
args = parser.parse_args()
searchedString1="667083570110.dkr.ecr.us-east-1.amazonaws.com/"
searchedString2="artifactory.kod.kyriba.com:443/"
now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

def getImage(line, searchedString):
    if re.search(searchedString, line):
        value = searchedString + line.split(searchedString)[1]
        value = value.split(" ")[0]

        if (re.search(r'\d+$', value)):
            return value
        if value.endswith("latest"):
            return value

    return ""

def fillImagesDict(url, data, dict):
    lines = data.splitlines()
    values = []
    for line in lines:
        found = True

        image = getImage(line, searchedString1)
        if not image:
            image = getImage(line, searchedString2)
        if image:
            values.append(image)

    if (len(values) > 0):
        valuesSet = set(values)
        dict[url]=list(valuesSet)


# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# For each build search for expected string in log
dict = {}
multiStagesTests = False
for build in builds:
    url = build['url']
    if "deployer" in url:
        data = commonBuilds.getBuildLog(url)
        fillImagesDict(url, data, dict)
    else:
        stages = commonBuilds.getStages(build)
        data = {}
        for stage in stages:
            if stage["name"] == "[test] Starting containers":
                multiStagesTests = True
                id = stage["id"]
                data = commonBuilds.getStageLog(build, stage)
                fillImagesDict("Test"+stage["id"], data, dict)
            #if multiStagesTests and (stage["name"] == "[test] Summary"):
                #name = commonBuilds.getTestName(build, stage)
                #fillImagesDict(name, data, dict)
                #data = {}

        if not multiStagesTests:
            url = build['url'].split("job/")[1].split("/")[0]
            data = commonBuilds.getBuildLog(build['url'])
            fillImagesDict(url, data, dict)

print (dict)