from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors

##################################################################################################################
##  Parametrized giving statistics on most consuming builds.                                                    ##
#   Sample:                                                                                                     ##
#   python GetTopConsumingJobs.py 'http://jenkyriba.kod.kyriba.com/' --numDays 10 --nameSize 20                 ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for always failing builds.",
                                 epilog="Sample: python GetBuildsStatistics.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--numRows", type=int, nargs='?', help="Number of considered days for build search", default=10)
parser.add_argument("--nameSize", type=int, nargs='?', help="size of considered name when grouping results", default=10)
parser.add_argument("--urlFilter", type=str, nargs='?', help="Only consider urls containing provided substring", default="")
parser.add_argument("--showBuilds", help="If specified each considered build will be displayed", action='store_true')

args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)
pd.set_option('display.max_rows', None)
CONSIDERED_STATUS = ['SUCCESS', 'FAILURE', 'UNSTABLE', 'ABORTED']
DISPLAYED_ROWS = args.numRows
DISPLAYED_COLUMN_SIZE = 100

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# Display distribution of found string
print ()
if builds:
    df = pd.json_normalize(builds)
    if args.urlFilter:
        df = df[df['url'].str.contains(args.urlFilter)]

    if args.showBuilds:
        print (df[["url", "name", "result"]])

    df['name'] = df['name'].str[:args.nameSize]
    buildCounter = df.groupby('name').size()
    buildCountersPerStatus = {}
    for status in CONSIDERED_STATUS:
        buildCountersPerStatus[status] = df[df['result'] == status].groupby('name').size()

    buildSuccessCounter = df[df['result'] == 'SUCCESS'].groupby('name').size()
    buildFailureCounter = df[df['result'] == 'FAILURE'].groupby('name').size()
    buildUnstableCounter = df[df['result'] == 'UNSTABLE'].groupby('name').size()
    buildAbortedCounter = df[df['result'] == 'ABORTED'].groupby('name').size()

    # Display most important statistics on found builds
    # 1 Total number of existing builds under existing url
    # 2 Percentage of build per category
    print('----------------------------')
    total = buildCounter.sum()
    print('Total number of builds: ', buildCounter.sum())
    for name, counter in buildCountersPerStatus.items():
        print('  - ', 'Percentage of ', name, ':', round(counter.sum() * 100 / total, 1), '%')
    print('----------------------------')

    # Display most frequent builds
    print('Most frequent builds:')
    for name, size in buildCounter.sort_values(ascending=False).head(DISPLAYED_ROWS).items():
        print('  - ', name.ljust(DISPLAYED_COLUMN_SIZE), ': ', size)
    print('----------------------------')


    # Most time consuming builds
    print('Most time consuming builds')
    buildDurations = df.groupby('name')['duration'].sum().sort_values(ascending=False)
    for name, size in buildDurations.head(DISPLAYED_ROWS).items():
        print('  - ', name.ljust(DISPLAYED_COLUMN_SIZE), ': ', round(size/3600000, 2), ' hours')
    print('----------------------------')

    # Display most useless builds
    # 1. categorized by number of execution
    # 2. categorized by duration
    print('Most useless builds categorized by number of execution:')
    names = []
    sizes = []
    durations = []
    for name, size in buildCounter.items():
        success = buildSuccessCounter.get(name) or 0
        if (success == 0):
            names.append(name)
            sizes.append(size)
            durations.append(buildDurations.get(name))

    dfUseless = pd.DataFrame(data = {'name': names, 'size': sizes, 'duration': durations})
    for index, row in dfUseless.sort_values(by=['size'], ascending=False).head(DISPLAYED_ROWS).iterrows():
        print('  - ', row['name'].ljust(DISPLAYED_COLUMN_SIZE), ': ', row['size'])
    print('----------------------------')
    print('Most useless builds categorized by consumed time:')
    for index, row in dfUseless.sort_values(by=['duration'], ascending=False).head(DISPLAYED_ROWS).iterrows():
        print('  - ', row['name'].ljust(DISPLAYED_COLUMN_SIZE), ': ', round(row['duration']/3600000, 2), ' hours')

    # Display never executed jobs (to be done)

else:
    print ("No element found")