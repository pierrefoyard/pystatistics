> Please find in this repository some python scripts generating KPIs to monitor software factory quality.
> These scripts are invoking REST Api to retrieve data so they can be time consuming.
> Scripts in Plot folder are returning charts. So this is recommended to run them in Jupyter Notebook to view result.
> Scripts outside of Plot folder can ben executed in Jupyter Notebook or directly in command line (using python command).

# Samples:

*Retrieve help about GrepDockerHub.py command:*
> python GrepDockerHub.py --help

*Retrieve list of "docker.io" references in ksink8s jenkins repo these last 10 days:*
> python GrepDockerHub.py "https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/" --numDays 10

*Retrieve list of exceptions in ksink8s jenkins repo these last 10 days:*
> python GrepDockerHub.py "https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/" --numDays 10 --searchedString Exception

*Retrieve distribution of errors from logs in Jenkyriba failing build these last 10 days:*
> python GetKnowLogErrors.py 'http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline' --numDays 10

*Retrieve pie chart of distribution of errors from logs in Jenkyriba failing build these last 10 days:*
> python GetKnowLogErrors.py 'http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline' --numDays 10 --plot

*Display success rate in bar chart of each integration test these last 10 days:*
> python GetIntegrationTestsDistribution.py --numDays 10 --plot

*Display distribution of failing stages in pie chart these last 10 days for KApp PR:*
> python GetFailingStages.py http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline --numDays 10 --plot

*Display distribution of failing stages in pie chart these last 10 days for KApp PR:*
> GetBuildStatusDistribution.py http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline --numDays 20 --plot


#You may some pip install to perform to get correct set of libraries
```
$ pip list
Package            Version
------------------ ---------
certifi            2021.10.8
charset-normalizer 2.0.10
cycler             0.11.0
fonttools          4.28.5
idna               3.3
kiwisolver         1.3.2
matplotlib         3.5.1
numpy              1.22.0
packaging          21.3
pandas             1.3.5
Pillow             9.0.0
pip                21.3.1
pyparsing          3.0.6
python-dateutil    2.8.2
pytz               2021.3
requests           2.27.1
setuptools         59.2.0
six                1.16.0
urllib3            1.26.8
wheel              0.37.0
```

# Install all dependencies with requirements.txt
$ pip install -r requirements.txt
