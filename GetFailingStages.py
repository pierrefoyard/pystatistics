from collections import Counter
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script searching for failing stages in a Jenkins url.                                          ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GrepDockerHub.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' \    ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for failing stage statistics in every underlying Jenkins build.",
                                 epilog="Sample: python GetFailingStages.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "FAILURE", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "FAILURE", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "FAILURE", url, startDate, now)
    builds.extend(urlBuilds)

# For each build retrieve list of stages
for build in builds:
    commonBuilds.enrichBuildWithFailingStage(build)

for build in builds:
    print (build['url'], ' --> ', build['failingStage'])

commonBuilds.showProgress (str(len(builds)) + " elements found")

# Display distribution of found string
print ()
if builds:
    df = pd.json_normalize(builds)
    tagsDistribution = df.groupby('failingStage').size().sort_values(ascending=True)
    print (tagsDistribution)

    # Plot result if required
    if args.plot:
        plot.displayAsPieChart("FAILING STAGES",
                                tagsDistribution,
                                tagsDistribution.index.values,
                                commonColors.getLabelsColors(tagsDistribution.index.values,
                                                            tagsDistribution.index.values))
else:
    print ("No element found")