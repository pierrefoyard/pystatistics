#!/bin/bash

# Create temp dir
tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
echo $tmp_dir

# Copy files from AWS S3
aws s3 cp s3://kyriba-depot/automation-logs/22.6/Financial-transactions/ $tmp_dir/ --recursive
#cp -rp /Users/pierre.foyard/Projects/pystatistics/testReport/* $tmp_dir/

# Loop on files
for pathname in $(find $tmp_dir/ -name "*.json"); do

    # Search for DDT result in current file
    status=$(sed  's/.*ddt/ddt/g' $pathname | sed 's/\}.*//' | grep ddt | sed 's/.*status\"://' | sed 's/\"//g')
    printf "DDT result for file %s = %s\n" "$pathname" "$status"
done

# Delete tmp folder
rm -rf $tmp_dir
