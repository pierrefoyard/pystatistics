from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script searching for average build index for given job.                                        ##
#   For example let's imagine we trigger this script on this SCF jobs' url. Let's assume we detect that today   ##
#   2 builds have been executed on SCF one with build number 2 and one with build number 4. Then this script    ##
#   will return 3, which is the average value of build Index.                                                   ##
#   Then we can assume that this average value represents half of the maximum of builds before merging a PR.    ##
#   So in former example we could assume that average number of builds per PR is 6.                             ##
#   This measure is obviously more precise if we consider a lot of builds.                                      ##
#   Sample:                                                                                                     ##
#   python GetBuildStatusDistribution.py 'http://jenkyriba.kod.kyriba.com/' \                                   ##
#                           --numDays 10 \                                                                      ##
#                           --plot                                                                              ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for build status distribution.",
                                 epilog="Sample: python GrepBuildStatusDistribution.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=1)
parser.add_argument("--showBuilds", help="If specified each considered build will be displayed", action='store_true')
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', None)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# Display distribution of found string
print ()
if builds:
    buildIndexSum = 0
    countBuild = 0
    for build in builds:
        url = build['url']
        if (not 'trunk' in url):
            url = url.rstrip('/')
            url = url.split('/')[-1]
            buildIndex = int(url)
            if (buildIndex > 0):
                if (buildIndex > 100) and (("PR_" in build["url"]) or ("PR-" in build["url"])):
                    print(build['url'], " --> ", buildIndex)
                buildIndexSum = buildIndexSum + buildIndex
                countBuild = countBuild + 1
            else:
                print ("could not find index in build ", build['url'])

    print ("Average build index = ", round(buildIndexSum / countBuild, 2))

else:
    print ("No element found")