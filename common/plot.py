import matplotlib.pyplot as plt
import common.colors as commonColors

def percentage(part, whole):
  percentage = round(100 * float(part)/float(whole))
  return str(percentage) + "%"

def displayAsPieChart(title, data, labels, colors):
    print( "display plot")
    sumValues = sum(data)
    index = 0
    newLabels = []
    for label in labels:
        newLabels.append(label + ' (' + percentage(data[index], sumValues) + ')')
        index=index+1
    plt.pie(data, autopct='%1.0f%%', colors=colors, shadow=True, wedgeprops={'edgecolor' :'white'}, textprops={'fontsize': 12})
    plt.legend(newLabels, loc="center", bbox_to_anchor = (0.2, 0), prop={'size': 12})
    plt.title(title)
    fig = plt.gcf()
    fig.set_size_inches(10,10)
    plt.tight_layout()
    plt.show(block=True)
