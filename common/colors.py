import matplotlib.pyplot as plt

def getLabelsColors(labels, predefinedLabels):
    colors = []
    predefined = sorted(predefinedLabels)
    cmap = list(plt.cm.tab20.colors)
    if (len(predefined) > 20):
        cmap.extend(plt.cm.tab20b.colors)
    if (len(predefined) > 40):
        cmap.extend(plt.cm.tab20c.colors)

    for label in labels:
        if label in predefined:
            index = predefined.index(label)
            colors.append(cmap[index])
        else:
            colors.append('gray')
    return colors

def getSingleStatusColor(status):
    if status == 'FAILURE':
        return 'crimson'
    elif status == 'UNSTABLE':
        return 'sandybrown'
    elif status == 'SUCCESS':
        return 'mediumseagreen'
    else:
        return 'slategray'

def getStatusColors(statuses):
    colors = []
    for status in statuses:
        colors.append(getSingleStatusColor(status))
    return colors