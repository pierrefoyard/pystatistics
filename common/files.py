import hashlib
import os
import json

FOLDER = os.path.sep + ".." + os.path.sep + "tmp" + os.path.sep
MAX_FILES = 10


# Display temporary message representing current activity.
lastProgressLen = 0 # Please don't change. Global variable to cleanup progress message.
def showProgress(str):
    global lastProgressLen
    print (' ' * lastProgressLen, end="\r")
    lastProgressLen = len(str) + 3
    print (str + "...", end="\r")


# Function retrieving root jenkins url
def getRootUrl(url):
    return url.split(".com")[0] + '.com'

# Function simplifying an url containing indirections
def simplifyUrl(url):
    return getRootUrl(url) + '/job/' + url.split('/job/', 1)[1].rstrip('/')

# Function removing url header
def removeHeaderUrl(url):
    url = url.split(".com")[1]
    if url.startswith("/job/"):
        url = url[5:]
    return url

def getCacheFolder():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return dir_path + FOLDER

# Build path to cache data
def buildFilePath(action, status, url, startDate, endDate):
    urlMd5 = hashlib.md5(url.encode('utf-8')).hexdigest()
    startDateStr = startDate.strftime('%d%m%Y')
    endDateStr = endDate.strftime('%d%m%Y')
    return getCacheFolder() + action + "-" + urlMd5 + "-" + status + "-" + startDateStr + "-" + endDateStr

# Load cached data if exists
def loadIfExists(action, status, url, startDate, endDate):
    filePath = buildFilePath(action, status, url, startDate, endDate)
    try:
        with open(filePath) as f:
            showProgress("Found file "+ filePath + " reload it.")
            return json.load(f)
    except IOError:
        showProgress("Could not find cached process in file, perform full computation.")
        return ""

# Save cached data
def saveAsFile(data, action, status, url, startDate, endDate):
    cleanUpOldCaches()
    filePath = buildFilePath(action, status, url, startDate, endDate)
    os.makedirs(os.path.dirname(filePath), exist_ok=True)
    with open(filePath, 'w') as outfile:
        json.dump(data, outfile)
        showProgress (action + "has been saved in " + filePath)

# Internal function to retrieve sorted files in a dir
# Copied from https://www.henrykoch.de/en/python-remove-oldest-files-in-a-directory-only-a-defined-count-of-them-remains
def getSortedCache(path):
    mtime = lambda f: os.stat(os.path.join(path, f)).st_mtime
    return list(sorted(os.listdir(path), key=mtime))

# Remove older files from cache to avoid disk over consumption
def cleanUpOldCaches():
    dir = getCacheFolder()
    sortedList = getSortedCache(dir)
    delList = sortedList[0:(len(sortedList) - MAX_FILES)]
    for dfile in delList:
        showProgress ("Remove old cache " + dfile)
        os.remove(dir + dfile)
