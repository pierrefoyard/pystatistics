import datetime
from dateutil.relativedelta import *
import requests
import json
import time
import re

# Adapt upper case variables below depending of usage
MAX_DEPTH=5 # Maximum depth search in sub views
MAX_URL_TRUNCATE=2 # Maximum number of truncate done on url
DISPLAY_INSTRUMENTATION=True
BUILD_FIELDS='url,timestamp,result,name,builtOn,duration,actions[causes[upstreamUrl,upstreamBuild]]'
RUNNING_BUILDS_DETAILS='/computer/api/json?tree=computer[displayName,assignedLabels[name],monitorData[availablePhysicalMemory],executors[currentExecutable[url]],oneOffExecutors[currentExecutable[url]]]'
PENDING_BUILDS_DETAILS='/queue/api/json?tree=items[inQueueSince,task[url]]'
UNDERLYING_TESTS='/testReport/api/json?tree=suites[name,enclosingBlockNames,cases[status,className,name]]'

lastProgressLen = 0 # Please don't change. Global variable to cleanup progress message.

def getRequest(url):
    for i in [1,2,3]:
        try:
            response = requests.get(url)
            return response
        except requests.exceptions.RequestException as e:
            if i<3:
                showProgress("Retry on failing request " + url)
                time.sleep(1)
            else:
                print("Request exception", e, "for url", url)
                if hasattr(e, 'partial'):
                    return e.partial

    return ""

def getJsonRequest(url):
    response = getRequest(url)
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        return ""

    return response.json()

def getAllJobs(url):
    response = getJsonRequest(url + '/api/json?tree=jobs[name,url,]')

def displayInstrumentation(str):
    if DISPLAY_INSTRUMENTATION:
        now = datetime.datetime.now()
        nowStr = now.strftime('%Y-%m-%d %H:%M:%S')
        print ("[", nowStr, "]", str)

def isSingleBuild(className):
    return className.endswith('Run') or className.endswith('Build')

def getProjectsMap(url):
    response = getJsonRequest(url + '/api/json?tree=jobs[displayName,url]')
    result = {}
    if 'jobs' in response:
        for job in response['jobs']:
            result[job['displayName']] = job['url']
    return result

def getLastBuild(url):
    response = getJsonRequest(url + '/api/json?tree=lastBuild[url]')
    if response and "lastBuild" in response:
        return response["lastBuild"]["url"]
    return ""

def getUnderlyingTests(url):
    response = getJsonRequest(url + UNDERLYING_TESTS)
    return response

# Drill down to underlying builds
def getDetails(url, className, jobName, buildFields):
    builds = []
    if (className.endswith('.Folder')
        or className.endswith('.OrganizationFolder')
        or className.endswith('.Hudson')
        or className.endswith('View')
        or className.endswith('.WorkflowMultiBranchProject')):
        # Current level has sub jobs:
        response = getJsonRequest(url + '/api/json?tree=jobs[fullName,url,allBuilds[' + buildFields + ']]')
        for job in response["jobs"]:
            if 'allBuilds' in job:
                for build in job['allBuilds']:
                    build['name'] = job['fullName'].split('/')[0]
                    setParentUpStreamUrl(build)
                    builds.append(build)
            else:
                builds.extend(getDetails(job['url'], job['_class'], job['fullName'].split('/')[0], buildFields))
    elif (className.endswith('Job')):
        # Current level has sub builds
        response = getJsonRequest(url + '/api/json?tree=fullName,allBuilds[' + buildFields + ']')
        for build in response['allBuilds']:
            build['name'] = response['fullName'].split('/')[0]
            setParentUpStreamUrl(build)
            builds.append(build)
    elif isSingleBuild(className):
        # Current level is a build
        response = getJsonRequest(url + '/api/json?tree=' + buildFields)
        response['name'] = jobName
        setParentUpStreamUrl(response)
        builds.append(response)
    elif className.endswith(".FreeStyleProject"):
        # Current build may have downstream elements or builds
        response = getJsonRequest(url + '/api/json?tree=name,downstreamProjects[' + buildFields + '],allBuilds[' + buildFields +']')
        if "downstreamProjects" in response and response["downstreamProjects"]:
            for downstreamProject in response["downstreamProjects"]:
                subBuilds = getDetails(downstreamProject['url'], downstreamProject['_class'], downstreamProject['name'], buildFields)
                builds.extend(subBuilds)
        if "allBuilds" in response and response["allBuilds"]:
            for build in response['allBuilds']:
                build['name'] = response['name']
                setParentUpStreamUrl(build)
                builds.append(build)
    else:
        print ("Unkown class type ", className, "for url ", url)

    return builds

def setParentUpStreamUrl(build):
    # build may have multiples actions and causes.
    # search for first one with an upstreamUrl.
    found = False
    if "actions" in build:
        actions  = build["actions"]
        for action in actions:
            if "causes" in action:
                causes = action["causes"]
                for cause in causes:
                    if ("upstreamUrl" in cause) and ("upstreamBuild" in cause):
                        url = build["url"].split(".com")[0]
                        url = url + '.com/' + cause["upstreamUrl"]
                        if not url.endswith('/'):
                            url = url + '/'
                        url = url + str(cause["upstreamBuild"])
                        build["upstreamUrl"] = url.rstrip('/')
                        found = True
                        break
            if found:
                break

def getUrlInfo(url):
    response = getJsonRequest(url + '/api/json?tree=name,fullDisplayName')
    if not response:
        print ("No information found for this url", url + '/api/json?tree=name')
        response = {}
    if not "name" in response:
        if "fullDisplayName" in response:
            response["name"] = response["fullDisplayName"]
        else:
            response["name"] = "main"

    return response

# Function retrieving list of Jenkins jobs with details on underlying stages
def getBuilds(url, status, startDate):
    elements = []
    info = getUrlInfo(url)
    if ('_class' in info):
        builds = getDetails(url, info['_class'], info['name'], BUILD_FIELDS)

        #print (str(len(builds)) +" existing builds in Jenkins")
        if ((len(builds) == 1) and isSingleBuild(info['_class'])):
            elements = builds
        else:
            for build in builds:
                buildDate = datetime.datetime.fromtimestamp(build['timestamp']//1000)
                build['buildDate'] = buildDate.strftime("%Y.%m.%d")
                if (buildDate >= startDate):
                    if (not status) or (build['result'] and build['result'] in status):
                        elements.append(build)

    #print ("Number of considered builds: ", len(elements))
    return elements
#   result = []
#   for elem in elements:
#       value = elem["url"].rstrip('/')
#       if (value.endswith("/trunk") or value.endswith("_PATCH") or ("/PR-" in value)):
#           result.append(elem)
#   return result

def getJenkinsActivity(url):
    showProgress ("Get list of running builds " + url)
    response = getJsonRequest(url + RUNNING_BUILDS_DETAILS)
    if (not response):
        print("Failed to retrieve list of running builds for url", url + RUNNING_BUILDS_DETAILS)
        return []
    else:
        return response

def getPendingBuilds(url):
    showProgress ("Get list of pending builds " + url)
    response = getJsonRequest(url + PENDING_BUILDS_DETAILS)
    if (not response):
        print("Failed to retrieve list of pending builds for url", url + RUNNING_BUILDS_DETAILS)
        return []
    else:
        return response

# Function retrieving full log and searching for single string in it
def getBuildLog(url):
    showProgress ("Analyze build " + url)
    result = ""
    response = getRequest(url + '/logText/progressiveText')
    time.sleep(1) # Wait to avoid network issues
    if hasattr(response, 'text'):
        return response.text
    else:
        return response

# Display temporary message representing current activity.
def showProgress(str):
    global lastProgressLen
    print (' ' * lastProgressLen, end="\r")
    lastProgressLen = len(str) + 3
    print (str + "...", end="\r")

# Retrieves build stages
def getStages(build):
    try:
        build["failingStage"] = 'Unknown'
        time.sleep(1) # Wait to avoid network issues
        showProgress ("retrieve stages for " + build['url'])
        response = getRequest(build['url']+'/wfapi/')
    except (requests.exceptions.ConnectionError, ValueError):
        return {}
    else:
        if not response:
            return {}
        stages = response.json()['stages']
        return stages

# Retrieves log of a specific stage
def getTestName(build, stage):
    try:
        url = build['url']
        splits1=url.split("/job/")
        url = splits1[0] + stage['_links']['self']["href"]
        response = getRequest(url)
    except (requests.exceptions.ConnectionError, ValueError):
        return {}
    else:
        if not response:
            return {}
        name = response.json()['stageFlowNodes'][-1]["parameterDescription"].split("summary")[0]
        return name


# Retrieves log of a specific stage
def getStageLog(build, stage):
    try:
        url = build['url']
        splits1=url.split("/job/")
        url = splits1[0] + stage['_links']['self']["href"]
        response = getRequest(url)
        logUrl = response.json()['stageFlowNodes'][-1]["_links"]["log"]["href"]
        url = splits1[0] + logUrl
        response = getRequest(url)
        time.sleep(1) # Wait to avoid network issues
    except (requests.exceptions.ConnectionError, ValueError):
        return {}
    else:
        if not response:
            return {}
        stages = response.json()['text']
        return stages

# Retrieve Jenkins build details as sub stages
def enrichBuildWithFailingStage(build):
    stages = getStages(build)
    for stage in stages:
        if stage['status']=='FAILED':
            build["failingStage"] = stage['name']
            break

def showExtraLog(data, index):
    start = index
    if (start > 100):
        start = start - 100
    print ()
    print ("------------------------------")
    print (data[start:index+100])
    print ("------------------------------")
