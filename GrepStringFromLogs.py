from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import re
import common.files as commonfiles
import common.plot as commonPlot
import common.colors as commonColors
from collections import Counter

##################################################################################################################
##  Parametrized script searching for provided string in a build, a job or a view.                              ##
#   It will return list of builds containing keyword and it will return list of elements words without space    ##
#   containing this keyword.                                                                                    ##
#   Sample:                                                                                                     ##
#   python GrepDockerHub.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' \    ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################


parser = argparse.ArgumentParser(description="Search for string usage in every underlying Jenkins build.\
                                              By default searched string is 'docker.io' and considered builds are \
                                              from one month only.",
                                 epilog="Sample: python GrepStringFromLog.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=0)
parser.add_argument("--numHours", type=int, nargs='?', help="Number of considered hours for build search", default=0)
parser.add_argument("--searchedString", type=str, nargs='?', help="Searched string", default="Exception")
parser.add_argument("--consideredBuildStatus", type=str, nargs='?', help="Restrict search to specified build status (SUCCESS/FAILURE...)", default="")
parser.add_argument("--caseInsensitive", help="If specified we will ignore case sensitivity", action='store_true')
parser.add_argument("--stopFirst", help="If specified we will stop at first found file", action='store_true')
parser.add_argument("--getStats", help="If specified we will stop at first found file", action='store_true')
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')

args = parser.parse_args()

now = datetime.datetime.now()
startDate = now
if (args.numDays):
    startDate = startDate + relativedelta(days=-args.numDays)

if (args.numHours):
    startDate = startDate + relativedelta(hours=-args.numHours)
print (startDate)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', args.consideredBuildStatus, url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, args.consideredBuildStatus, startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', args.consideredBuildStatus, url, startDate, now)
    builds.extend(urlBuilds)

# For each build search for expected string in log
values = []
for build in builds:
    data = commonBuilds.getBuildLog(build['url'])

    lines = data.splitlines()
    index=0
    for line in lines:
        index=index+1
        found = True
        if args.caseInsensitive:
            found = re.search(args.searchedString, line, re.IGNORECASE)
        else:
            found = re.search(args.searchedString, line)
        if found:
            values.append(line)
            #print (build['url'], " # ", round(build['duration']/6000)," --> ", line)
            print (build['url'], " line ", index, "--> ", line)
            if args.stopFirst:
                break

counter = Counter(values)
print (counter)
if args.plot:
    labels = list(counter.keys())
    stats = list(counter.values())

    commonPlot.displayAsPieChart("FAILING STAGES",
                            stats,
                            labels,
                            commonColors.getLabelsColors(labels,
                                                        labels))