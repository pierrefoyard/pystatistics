from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import re
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors

##################################################################################################################
##  Parametrized script searching for underlying builds of a build.                                             ##
#   For example if a build A triggers a build B and C, then if C triggers build D we will see:                  ##
#   A
#   |--B
#   |--C
#      |--D
#   Sample:                                                                                                     ##
#   python GetDownStreamProjects.py 'http://jenkyriba.kod.kyriba.com/job/feature_branch_trunk_merge_trigger' \  ##
##################################################################################################################

exit
parser = argparse.ArgumentParser(description="Search for always failing builds.",
                                 epilog="Sample: python GetBuildsStatistics.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=4)
args = parser.parse_args()
pd.set_option('display.max_rows', None)
JENKINS_URL_PREFIX="https://jenk"
CURL_COMMAND="curl -X POST "
now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Function searching underlying builds in logs
def getJenkinsUrlInvokation(url):
    data = commonBuilds.getBuildLog(url)
    lines = data.splitlines()
    result = []
    for line in lines:
        if JENKINS_URL_PREFIX in line:
            subUrl = JENKINS_URL_PREFIX + line.split(JENKINS_URL_PREFIX)[1]
            subUrl = re.split(';|,|\ ',subUrl)[0]
            subUrl = subUrl.rstrip(' ,/\'')
            if (CURL_COMMAND in line):
                subUrl = subUrl.split()[0]
                if (subUrl.endswith("/build")):
                    subUrl = subUrl[:-5]
                    subUrl = commonBuilds.getLastBuild(subUrl)
                    subUrl = subUrl.rstrip(' ,/\'')
            else:
                if not subUrl[-1].isdigit():
                    subUrl = ""

            if subUrl and (not subUrl in result):
                result.append(subUrl)
    return result

def addFoundUrl(setOfFoundElements, url):
    setOfFoundElements.add(url)
    index = url.rindex('/')
    if index > 0:
        setOfFoundElements.add(url[0:index])


# Function searching for any underlying build
def displaySubLevel(data, url, level, setOfFoundElements):
    if 'upstreamUrl' in data:
        subUrl = data[data['upstreamUrl'] == url]['url']
        for item in subUrl.items():
            subUrl = item[1].rstrip('/')

            if not subUrl in setOfFoundElements:
                addFoundUrl(setOfFoundElements, subUrl)
                print('|   '*level + '|--', subUrl.ljust(200))
                displaySubLevel(data, subUrl, level+1, setOfFoundElements)

    remoteUrls = getJenkinsUrlInvokation(url)
    for item in remoteUrls:
        item = commonfiles.simplifyUrl(item)
        if not item in setOfFoundElements:
            addFoundUrl(setOfFoundElements, item)
            print('|   '*level + '|--', item.ljust(200))
            otherJenkinsData = loadBuildsFromJenkinsRoot(item)
            displaySubLevel(otherJenkinsData, item, level+1, setOfFoundElements)


# Function loading dependencies between builds on a particular Jenkins
def loadBuildsFromJenkinsRoot(url):
    # Load from cache if exists
    builds = []
    parentUrl = commonfiles.getRootUrl(url)
    urlBuilds = commonfiles.loadIfExists('builds', "", parentUrl, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(parentUrl, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", parentUrl, startDate, now)
    builds.extend(urlBuilds)
    df = pd.json_normalize(builds)
    return df

pd.set_option('display.max_rows', None)

# Display underlying builds
df = loadBuildsFromJenkinsRoot(args.jenkinsUrl)
print('|--', args.jenkinsUrl.ljust(200))

url = commonfiles.simplifyUrl(args.jenkinsUrl)
setOfElements = set([])
addFoundUrl(setOfElements, url)
displaySubLevel(df, url, 1, setOfElements)
