from collections import Counter
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import *
import argparse
import re
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.plot as commonPlot
import common.colors as commonColors

##################################################################################################################
##  Parametrized script searching for typical errors in a build, a job or a view.                               ##
#   It will return distribution of found issues.                                                                ##
#   Sample:                                                                                                     ##
#   python GetKnowLogError.py 'http://jenkyriba.kod.kyriba.com/' \                                              ##
#                           --numDays 1 \                                                                       ##
#                           --plot                                                                              ##
##################################################################################################################

# Enrich in this list name of know errors
INTERNAL_FUNCTION_MARKER = 'INTERNAL_FUNCTION:'

FLAGS = {'falling back to nondeterministic checkout': 'Cannot checkout',
         'ERROR: Maximum checkout retry attempts reached, aborting': 'Cannot checkout',
         'ERROR: Caught error: No such property: branches for class: hudson.scm.NullSCM': 'Cannot checkout',
         'fatal: Remote branch .* not found in upstream origin': 'Cannot checkout',
         'Could not GET \'https://plugins-artifacts.gradle.org': 'Handshake error on public registry',
         'artifactory.kod.kyriba.com:443/v2/: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)': 'Artifactory timeout',
         'net/http: TLS handshake timeout': 'Artifactory(TLS handshake)',
         'tail: cannot open \'/opt/kyriba/logs/kyriba/front1_testjsp.log\'': 'cannot open front1_testjsp.log',
         'stderr: fatal: Couldn\'t find remote ref refs/heads': 'Jenkins job executed on deleted branch',
         '<message>Failed to parse': 'Compilation issue',
         'No space left on device': 'Empty disk space',
         'Pipeline aborted due to quality gate failure': 'Sonar Quality Gate',
         'Error: Cannot perform an interactive login from a non TTY device': 'Cannot log to artifactory',
         'ImportError: cannot import name docevents': 'AWS CLI Failed',
         'LOG:  autovacuum launcher shutting down' : 'KeyCloak postgresl init issue',
         'Keycloak env failed' : 'Cannot initialize Keycloak',
         'ChannelClosedException': 'Node revoked',
         'hudson.model.Node': 'Node revoked',
         'RemovedNodeListener': 'Node revoked',
         'java.lang.OutOfMemoryError: Java heap space': 'Out of memory',
         'Failed to publish publication ': 'Fail to push on artifactory',
         'InterruptedException': 'Node revoked',
         'Could not connect to i-': 'Node revoked',
         'Remote call on JNLP4-connect connection from ip-': 'Node revoked',
         'Error from server (NotFound): namespaces': 'Node revoked',
         'Required context class hudson.model.Node is missing': 'Node revoked',
         'Invalid login/password for user id minsk-qas@kyriba.com': 'Green Mail connection',
         'ssh_exchange_identification: Connection closed by remote host': 'Node revoked',
         'Cannot contact i-': 'Node revoked',
         'was marked offline: Node is being removed': 'Node revoked',
         'Failed in branch Create KS container': INTERNAL_FUNCTION_MARKER + 'grepKSContainer',
         'Task [sequential] failed after [12] attempts; giving up.': 'Error at deployment time: 12 failures in a row - to be investigated',
         'Task :Git:compileJava FAILED': 'Compilation issue',
         'com.cloudbees.jenkins.plugins.bitbucket.api.BitbucketRequestException: HTTP request error.': 'Cannot checkout',
         'error: git-remote-https died of signal 15': 'Cannot checkout',
         'There is no running FKRUN monitor (exit with code 2)': 'Test failure - Missing FKRUN referenced by Firco',
         'Cannot connect FKRUN monitor (exit with code 1)': 'Test failure - Missing FKRUN referenced by Firco',
         ': net/http: request canceled while waiting  for connection (Client.Timeout exceeded while awaiting headers)': 'Test failure - AWS issue',
         'Failed to get D-Bus connection: No such file or directory': 'Test failure - D-Bus',
         'cp: cannot stat \'/opt/kyriba/logs/kyriba/': 'Test failure - Upload logs',
         'does not implement ServerInterface': 'Compilation issue',
         '[ERROR] Oracle-xe can\'t start!': 'Test failure - Oracle container start-up',
         'error pulling image configuration: received unexpected HTTP status:': 'Test failure - Could not pull image',
         'tests completed, [1-9][0-9]* failed, [0-9]* skipped': 'Unit test failure',
         'There were failing tests. See the report at': 'Unit test failure',
         'ERROR: Caught error: URL: https://api.bitbucket.org': 'Cannot checkout',
         '[ERROR]: The 5 attempts to download the image': 'Could not download image',
         'Remote call on i-': 'Node revoked',
         'Automatic merge failed; fix conflicts and then commit the result': 'Git merge conflicts',
         '> Detected binary changes between ': 'API changed detected by JApiCmp',
         'The remote job did not succeed': 'Remote job failure (TS Build/Create docker image)',
         'ERROR: Remote build failed with \'ExceedRetryLimitException\'': 'Remote job failure (TS Build/Create docker image)',
         'Caught error: Could not determine exact tip revision of PR-' : 'Cannot checkout',
         'No such property: userRemoteConfigs for class: hudson.scm.NullSCM' : 'Jenkins - Git error',
         'None of the test reports contained any result' : 'Test failure - empty scope',
         '[test] Logs on error' : 'Test failure - unknown',
         'k8sBuilderJava::exception - script returned exit code': 'Compilation issue when building K8s component',
         '> Compilation failed; see the compiler error output for details.': 'Compilation issue',
         'err 0: ,,,,,': "Invalid helm configuration",
         "Execution failed for task ':.*:compile": 'Compilation issue',
         "Task :.*React.* FAILED": 'UI Compilation issue',
         "Execution failed for task ':.*:test": 'Unit test failure',
         "Execution failed for task ':.*SQL": 'Liquibase preparation issue',
         "Execution failed for task ':.*Sql": 'Liquibase preparation issue',
         "Execution failed for task ':": INTERNAL_FUNCTION_MARKER + 'grepFailedCompilationTask',
         'ERROR: pull-request_javaee_test_': 'JavaEE test failure',
         'Back-DB versions consistency check failed!': "Invalid refere",
         'DB was not updated successfully!': 'DB Upgrade issue',
         'FtpSendParam.csv': 'FtpSendParam.csv issue',
         'The 5 attempts to download the image 667083570110.dkr.ecr.us-east-1.amazonaws.com/.*oracle-xe.*failed': 'Too old branch',
         '0-pull-request ts assembly .* completed with status FAILURE \(propagate: false to ignore\)': INTERNAL_FUNCTION_MARKER + 'grepTSCompilation',
         'gradlew: Argument list too long': 'Invalid gradle command line',
         'Jira issue is closed': 'Jira issue is closed'
       }

KS_CONTAINER_FLAGS = {
    'occurred while executing this line:", "/opt/kyriba/deployment/current/scripts/ant/ldu_targets.xml': 'ADS wrong configuration'
}

parser = argparse.ArgumentParser(description="Search for typical errors in Jenkins logs.\
                                              This mechanism can search in build, job or view.",
                                 epilog="Sample: python GetKnowLogError.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=0)
parser.add_argument("--numHours", type=int, nargs='?', help="Number of considered hours for build search", default=0)
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')
parser.add_argument("--showErrors", help="If specified then part of error message will be displayed in logs", action='store_true')
parser.add_argument("--consideredBuildStatus", type=str, nargs='?', help="Restrict search to specified build status (SUCCESS/FAILURE...)", default="FAILURE")
args = parser.parse_args()


def extractValue(line, build, tag):
    if (tag.startswith(INTERNAL_FUNCTION_MARKER)):
        funcName = tag.lstrip(INTERNAL_FUNCTION_MARKER)
        func = globals()[funcName]
        return func(line, build)
    else:
        return tag

def grepKSContainer(line, build):
    # Search for underlying build preparing KS container
    ksContainerBuilds = commonBuilds.getBuilds('https://jenkins-prod-0.1-kapp.svc.k8s.kod.kyriba.com/job/create_ks_21cXE_container', 'FAILURE', datetime.datetime.fromtimestamp(build['timestamp']//1000))
    for ksContainerBuild in ksContainerBuilds:
        if ksContainerBuild['upstreamUrl'].rstrip('/')==build['url'].rstrip('/'):
            print("found --> ", ksContainerBuild['url'])
            ksContainerTag = grepForPattern(ksContainerBuild, KS_CONTAINER_FLAGS)
            if (ksContainerTag):
                return ksContainerTag
            break
    return 'Issue at KS container creation'

def grepTSCompilation(line, build):
    # Search for underlying build compiling TS
    tsCompilationBuilds = commonBuilds.getBuilds('https://jenkins-prod-0.1-kapp.svc.k8s.kod.kyriba.com/job/pull-request_ts_assembly', 'FAILURE', datetime.datetime.fromtimestamp(build['timestamp']//1000))
    for tsCompilationBuild in tsCompilationBuilds:
        if tsCompilationBuild['upstreamUrl'].rstrip('/')==build['url'].rstrip('/'):
            tsCompilationTag = grepForPattern(tsCompilationBuild, FLAGS)
            if (tsCompilationTag):
                return tsCompilationTag
            break
    return 'Issue at TS compilation'

def grepFailedCompilationTask(line, build):
    return 'Compilation issue on ' + line.split(':')[-1]

def grepTestJspError(lines):
    message = 'Error when starting containers'
    for res in reversed(lines):
        if ('<target-node type="startup">' in res):
            return message
        elif '<message>' in res:
            message = res.lstrip("'").lstrip().lstrip('<message>').rstrip('</message>')
    return message

def grepForPattern(build, tagMap):
    data = commonBuilds.getBuildLog(build['url'])
    result = 'Unknown'

    lines = data.splitlines()
    for line in reversed(lines):
        for key in tagMap:
            if (re.search(key, line)):
                result = extractValue(line, build, tagMap[key])
                if args.showErrors:
                    result = result + " --> " + line
                return result

    # Special case when testJsp failed
    if ('Build on ks-docker11 is not AVAILABLE!' in data):
        result = grepTestJspError(lines)

    return result

now = datetime.datetime.now()
startDate = now
if (args.numDays):
    startDate = startDate + relativedelta(days=-args.numDays)

if (args.numHours):
    startDate = startDate + relativedelta(hours=-args.numHours)


# Retrieve list of considered builds
builds = []

for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', args.consideredBuildStatus, url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, args.consideredBuildStatus, startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', args.consideredBuildStatus, url, startDate, now)
    builds.extend(urlBuilds)

# For each build search for expected string in log
for build in builds:
    build["tag"] = grepForPattern(build, FLAGS)
    print(build['url'], ' --> ', build["tag"].ljust(100))

# Display distribution of found string
print ()
if builds and not args.showErrors:
    df = pd.json_normalize(builds)
    df.rename(columns={'tag':'Distribution:'})
    tagsDistribution = df.groupby('tag').size().sort_values(ascending=True)
    print (tagsDistribution)

    # Plot result if required
    if args.plot:
        values = []
        pairs = FLAGS.items()
        for key, value in pairs:
            values.append(value)

        commonPlot.displayAsPieChart("BUILD STATUS",
                                    tagsDistribution,
                                    tagsDistribution.index.values,
                                    commonColors.getLabelsColors(tagsDistribution.index.values, values))

else:
    print ("No element found")
