from collections import Counter
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script searching for failing stages in a Jenkins url.                                          ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GrepDockerHub.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' \    ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for failing stage statistics in every underlying Jenkins build.",
                                 epilog="Sample: python GetFailingStages.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "FAILURE", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "FAILURE", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "FAILURE", url, startDate, now)
    builds.extend(urlBuilds)

# For each build retrieve list of stages
for build in builds:
    stages = commonBuilds.getStages(build)
    duration = 0
    for stage in stages:
        if (stage['name'].startswith('[test]')):
            duration = duration + stage['durationMillis']
    print (build['url'], ' --> ', duration//1000, 's')