from collections import Counter
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script searching for failing stages in a Jenkins url.                                          ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GrepDockerHub.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' \    ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for failing stage statistics in every underlying Jenkins build.",
                                 epilog="Sample: python GetFailingStages.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# For each build retrieve list of stages
numSuccess=0
numUnstable=0
numFailure=0
numAborted=0
for build in builds:
    stages = commonBuilds.getStages(build)
    for stage in stages:
        if stage['name'] == "[test] Collect PTL reports":
            if stage['status'] == 'UNSTABLE':
                print (build["url"], "-->", stage['status'].ljust(100))
                numUnstable=numUnstable+1
            elif stage['status'] == 'FAILURE':
                numFailure=numFailure+1
            elif stage['status'] == 'SUCCESS':
                numSuccess=numSuccess+1
            elif stage['status'] == 'ABORTED':
                numAborted=numAborted+1
            break

print("Success:", numSuccess, "".ljust(100))
print("Failure:", numFailure, "".ljust(100))
print("Aborted:", numAborted, "".ljust(100))
print("Unstable:", numUnstable, "".ljust(100))