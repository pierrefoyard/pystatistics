from collections import Counter
import datetime
import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles

##################################################################################################################
##  Parametrized script searching for underlying failing tests in integrator.                                   ##
#   It will return percentage of failing builds for each test.                                                  ##
#   Sample:                                                                                                     ##
#   python GetIntegrationTestsDistribution.py 'http://jenkyriba.kod.kyriba.com/' \                              ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Retrieve ratio of success builds in integration.",
                                 epilog="Sample: python GetIntegrationTestsDistribution.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--plot", help="If specified bar chart will be displayed to represent distribution", action='store_true')
args = parser.parse_args()

consideredJenkinsUrls = ['http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_useracceptance_gen',
                        'http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_regression_gen',
                        'http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_ddt_gen']

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []
for url in consideredJenkinsUrls:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, '', startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# Display distribution of found string
print ()
if builds:
    print (builds)
    df = pd.json_normalize(builds)
    buildCounter = df.groupby('name').size()

    print (buildCounter)
    buildSuccessCounter = df[df['result'] == 'SUCCESS'].groupby('name').size()
    results = []
    for name, size in buildCounter.items():
        result =  {}
        result["name"] = name
        success = buildSuccessCounter.get(name)
        if size == 0:
            result["ratio"] = 0
        else:
            result["ratio"] = 100*success / size
        results.append(result)

    df = pd.DataFrame.from_dict(results)
    df = df.set_index('name')
    df = df.sort_values(by=['ratio'])
    df["total"]=100-df['ratio']

    print (df)

    if args.plot:
        df.head(20).plot.barh(title="Tests top offenders", stacked=True, color=['cornflowerblue', 'pink'])
        plt.show(block=True)