import csv
import argparse
import pandas as pd
import json

CUSTOMER_NAME = 'CORP_CUSTOMER_ID'
PT_COL_NAME = 'TEMPLATE_TYPE_CODE'
PARAM_COL_NAME = 'CLOB_TEMPLATE_OBJECT'
STREAM  ='stream'
UNIT = 'unit-class'

#Define parameters
parser = argparse.ArgumentParser(description="Read provided file to extract Process Templates parameters.",
                                 epilog="Sample: python parsePTs.py './file.csv'")
parser.add_argument("paths", type=str, nargs='+', help="List of paths")
args = parser.parse_args()

# Function to extract fake panel name
def buildPanelName(unit, index):
    return "Panel" + str(index) + "-" + unit.split(".")[-1].rstrip("Unit").rstrip("Template")

# Recursive function extracting values stored at different stream depth
def getUnderlyingValues(elem):
    result = []
    if STREAM in elem:
        for subElem in elem[STREAM]:
            if isinstance(subElem, dict):
                result += getUnderlyingValues(subElem)
            else:
                result.append(subElem)
    return result


# Function to extract data from CLOB json
def formatParams(paramJson):
    result = {}
    index = 1
    for elem in paramJson[STREAM]:
        if isinstance(elem, dict):
            # Build fake panel name
            panelName = buildPanelName(elem[UNIT], index)
            index = index + 1

            # Store single values
            panelValues = getUnderlyingValues(elem)
            if len(panelValues) > 0:
                result[panelName] = '-'.join(str(e) for e in panelValues)
    return result


# Import CSV and extract Customer, PT name and parameters as json
arrayValues = []
for path in args.paths:
    with open(path)  as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            rowJson = {CUSTOMER_NAME: row[CUSTOMER_NAME], PT_COL_NAME: row[PT_COL_NAME]}
            params = {}
            paramsString = row[PARAM_COL_NAME]
            if paramsString:
                params = json.loads(paramsString)
                formattedParams = formatParams(params)
                for key, value in formattedParams.items():
                    rowJson[key] = value

            arrayValues.append(rowJson)

# Build dataframe from extracted data
df = pd.DataFrame(arrayValues)

# print information about most represented elements
print("------------ Number of PTs per customer (top 10) ------------")
print(df.groupby(CUSTOMER_NAME).size().sort_values(ascending=False).head(10))
print("------------ Number of represented PT Type Code (top 10) ------------")
print(df.groupby(PT_COL_NAME).size().sort_values(ascending=False).head(10))
print("------------ Number of Distinct PT TYPE Code per customer (top 10) ------------")
print(df.groupby(CUSTOMER_NAME)[PT_COL_NAME].nunique().sort_values(ascending=False).head(10))
print("------------ Most represented parameters for CASH06 ------------")
cash06Df = df[df[PT_COL_NAME]=='CASH06']
for column in cash06Df:
    if column.startswith("Panel"):
        if not cash06Df[column].isnull().all():
            print('- Most common values for panel', column)
            print(cash06Df.groupby(column).size().sort_values(ascending=False).head(10))