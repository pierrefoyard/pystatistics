from collections import Counter
import matplotlib.pyplot as plt
from datetime import datetime
import seaborn as sns
import pandas
import requests
import json
import time
import matplotlib.patches as mpatches
# Adapt upper case variables below depending of usage
MAX_JOBS=False # Max number of considered jobs
RELOAD_FROM_JENKINS = True # Set to false if you want to rely on preloaded file
START_DATE = datetime.strptime("01/12/2021", "%d/%m/%Y") # set "startDate = 0" if you want to ignore it
END_DATE = datetime.strptime("15/01/2022", "%d/%m/%Y") # set "endDate = 0" if you want to ignore it
TEMP_STORAGE_FILE = '/Users/pierre.foyard/Downloads/jenkinsKAppPRLogs.json'
JENKINS_URL = 'http://jenkyriba.kod.kyriba.com/job/KApp_pull_request_pipeline'

FLAGS = {'Pipeline aborted due to quality gate failure': 'Sonar Quality Gate',
         'InterruptedException': 'Node revoked',
         'Could not connect to i-': 'Node revoked',
         'ssh_exchange_identification: Connection closed by remote host': 'Node revoked',
         'Cannot contact i-': 'Node revoked',         
         'Execution failed for task \':Git:test': 'Functional Class itest',
         'Task :Git:cash-back:test FAILED': 'Functional Class itest',
         'Failed in branch Create KS container': 'ADS issue at KS container creation',
         'TLS handshake timeout': 'Artifactory(TLS handshake)',
         'Task [sequential] failed after [12] attempts; giving up.': 'Test failure - KS deployment',
         'Task :Git:compileJava FAILED': 'Compilation issue',         
         'ERROR: Caught error: 0-pull-request ts assembly': 'TS assembly failed',
         'com.cloudbees.jenkins.plugins.bitbucket.api.BitbucketRequestException: HTTP request error.': 'Bitbucket',
         'There is no running FKRUN monitor (exit with code 2)': 'Test failure - Missing FKRUN referenced by Firco',
         'Cannot connect FKRUN monitor (exit with code 1)': 'Test failure - Missing FKRUN referenced by Firco',
         'An error occurred (ImageNotFoundException) when calling the DescribeImages operation: The image with imageId': 'Test failure - Image not found'
        }

pandas.set_option('display.max_rows', None)
TITLE_SIZE = 50
ratioSuccess = 0
ratioFailure = 0
ratioUnstable = 0
rationCompilation = 0
ratioTestSetup = 0
ratioBuilDocker = 0
ratioSonar=0

# Function retrieving list of Jenkins jobs
def getJobs(url):    
    response = requests.request("GET", url + '/api/json?tree=jobs[name,url,builds[url,timestamp,result]]')    
    data = response.json()['jobs']    
    
    if MAX_JOBS > 0:        
        return data[0:MAX_JOBS]
    else:
        return data


# Test if given job contains at list one build in given date interval
def isJobInDateFrame(job, startDate, endDate):
    if (not startDate) and (not endDate):
        return True
    else:
        for build in job['builds']:
            buildDate = datetime.fromtimestamp(build['timestamp']//1000)
            startDateIsValid = (not startDate) or (buildDate >= startDate)
            endDateIsValid = (not endDate) or (buildDate <= endDate)
            if (startDateIsValid and endDateIsValid):
                return True
    return False
            
# Function retrieving list of Jenkins jobs with details on underlying stages
def getJobsWithDetails(url, startDate, endDate):
    jobs = getJobs(url)
    result = {}
    result['builds'] = []
    if startDate:
        result['startDate'] = startDate.strftime("%d/%m/%Y")
    if endDate:
        result['endDate'] = endDate.strftime("%d/%m/%Y")

    result['url'] = url

    for job in jobs:
        if (isJobInDateFrame(job, startDate, endDate)):
            for build in job['builds']:
                if (build['result'] == 'FAILURE'):
                    build['tag'] = enrichWithMatchingLogs(build['url'])
                    result['builds'].append(build)
            
    return result  


def default(elements, key):
    if (key in elements):
        return elements[key]
    else:
        return 0

def enrichWithMatchingLogs(url):
    time.sleep(1) # Wait to avoid network issues
    response = requests.request("GET", url + 'logText/progressiveText?start=0')    
    #data = '\n'.join(response.text.splitlines()[-MAX_ROW_SEARCH_IN_LOGS:])
    data = response.text
    found = False
    for key in FLAGS:
        if key in data:
            result = FLAGS[key]
            found = True
            break
    if not found:
        result = 'Unknown'
    print(url, ' --> ', result)
    return result
    
    
    
##################################################
##                  MAIN CODE                   ##
##################################################
if RELOAD_FROM_JENKINS == True:
    data = getJobsWithDetails(JENKINS_URL, START_DATE, END_DATE)
    with open(TEMP_STORAGE_FILE, 'w') as outfile:
        json.dump(data, outfile)

with open(TEMP_STORAGE_FILE) as f:
    d = json.load(f)
buildsNormalized = pandas.json_normalize(d, record_path ='builds')

tagsDistribution = buildsNormalized.groupby('tag').size().sort_values(ascending=False)
print ('Log tags distribution')
plt.pie(tagsDistribution, labels=tagsDistribution.index, autopct='%1.0f%%', shadow=True)
fig = plt.gcf()
fig.set_size_inches(10,10)
plt.tight_layout()

#print (buildsNormalized)