from collections import Counter
import matplotlib.pyplot as plt
import seaborn as sns
import pandas
import requests
import json
import time
import matplotlib.patches as mpatches
# Adapt variables below depending of usage
maxConsideredJobs=50 # Max number of considered jobs
reloadFromJenkins = False # Set to false if you want to rely on preloaded file
tempStorageFile = '/Users/pierre.foyard/Downloads/jenkinsKSTests.json'
consideredJenkinsUrls = ['http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_useracceptance_gen',
                        'http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_regression_gen',
                        'http://jenkyriba.kod.kyriba.com/view/2_KS_trunk-integrate/job/trunk-integrate_ddt_gen']
pandas.set_option('display.max_rows', None)

# Function retrieving list of Jenkins builds
def getBuilds(url):
    response = requests.request("GET", url + '/api/json?tree=builds[url,result]')    
    data = response.json()
    if maxConsideredJobs > 0:        
        return data['builds'][0:maxConsideredJobs]
    else:
        return data['builds']

def getDownstreamProjects(url):
    response = requests.request("GET", url + '/api/json?tree=downstreamProjects[name,url]')    
    data = response.json()
    return data['downstreamProjects']

def getDownstreamProjectsWithDetails(urls):
    tests = []
    for url in urls:
        testTypes = getDownstreamProjects(url)
        for testType in testTypes:        
            time.sleep(1) # Wait to avoid network issues
            builds = getBuilds(testType['url'])
            for build in builds:
                test = {}
                test['name'] = testType['name']
                test['url'] = build['url']
                test['result'] = build['result']
                tests.append(test)
    return tests

##################################################
##                  MAIN CODE                   ##
##################################################
if reloadFromJenkins == True:
    data = getDownstreamProjectsWithDetails(consideredJenkinsUrls)
    with open(tempStorageFile, 'w') as outfile:
        json.dump(data, outfile)

with open(tempStorageFile) as f:
    d = json.load(f)

buildsNormalized = pandas.json_normalize(d)
buildCounter = buildsNormalized.groupby('name').size()
buildSuccessCounter = buildsNormalized[buildsNormalized['result'] == 'SUCCESS'].groupby('name').size()
results = []
for name, size in buildCounter.items():
    result =  {}
    result["name"] = name
    success = buildSuccessCounter.get(name)
    if size == 0:
        result["ratio"] = 0
    else:
        result["ratio"] = 100*success / size
    results.append(result)

df = pandas.DataFrame.from_dict(results)
#df.reset_index(level='name', drop=True)
df = df.set_index('name')
df = df.sort_values(by=['ratio'])
df["total"]=100-df['ratio']

print (df)

df.head(20).plot.barh(title="Tests top offenders", stacked=True, color=['cornflowerblue', 'pink'])
plt.show()

print ("Average ratio of success:")
print ("-------------------------")
print (round(df["ratio"].mean(), 2))


