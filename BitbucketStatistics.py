from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot
import json
from datetime import datetime
import os

##################################################################################################################
##  Parametrized script searching Bitbucket statistics.                                                         ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GetDownStreamProjects.py 'https://bitbucket.org/!api/2.0/repositories/kyridev/kyriba-app'            ##
##################################################################################################################
parser = argparse.ArgumentParser(description="Search for failing stage statistics in every underlying Jenkins build.",
                                 epilog="GetDownStreamProjects.py 'https://bitbucket.org/!api/2.0/repositories/kyridev/kyriba-app'")
parser.add_argument("paths", type=str, nargs='+', help="List of paths to retrieve considered elements")
args = parser.parse_args()

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', None)
KYRIDEV_API = 'https://api.bitbucket.org/2.0/repositories/kyridev/'
PULL_REQUEST_API = '/pullrequests?pagelen=50&fields=+values.participants&q=state="OPEN"&page='

# Use this function to retrieve list of pending pull requests from bitbucket rest API
# Does not work fine for the moment (looks to require login)
def loadPullRequestsFromBB(path):
    data = []
    # loops on PR pages (with max number of 10 pages)
    for index in range(10):
        response = commonBuilds.getJsonRequest(KYRIDEV_API + path + PULL_REQUEST_API + str(index+1))
        print ("request result =", response)
        if response and ("values" in response):
            data = data + response["values"]
        else:
            break
    return data

# Use this function to retrieve list of pending pull requests from files in given paths
def loadPullRequestsFromFiles(path):
    data = []
    try:
        for root, dirs, files in os.walk(path):
             for file in files:
                if (file.endswith(".json")):
                    with open(os.path.join(root, file), "r") as f:
                        jsonFile = json.load(f)
                        if jsonFile and ("values" in jsonFile):
                            data = data + jsonFile["values"]
    except IOError:
        print("Could not find file")
    return data

data = []
for path in args.paths:
    # Direct rest API does not look to work fine for now --> to be investigated. Until that we need to execute it locallyu
    #data.append(loadPullRequestsFromBB(path))
    data = data + loadPullRequestsFromFiles(path)

pullRequests = []
approvers = []
for value in data:
    pr = {}
    if 'author' in value:
        pr['author'] = value['author']['display_name']
        pr['branch'] = value['source']['branch']['name']
        pr['creationDate'] = datetime. strptime(value['created_on'].split("T")[0], '%Y-%m-%d')
        pr['reviewers'] = 0
        pr['approvers'] = 0
        pr['close_source_branch'] = value['close_source_branch']
        pr['state'] = value['state']
        for participant in value['participants']:
            element = {}
            element['name'] = participant['user']['display_name']
            element['approved'] = participant['approved']
            pr['reviewers'] = pr['reviewers'] + 1
            if element['approved'] == True:
                pr['approvers'] = pr['approvers'] + 1
            approvers.append(element)
        pullRequests.append(pr)

dfPR = pd.DataFrame(pullRequests)
dfApprovers = pd.DataFrame(approvers)
print (dfPR)


print("Number of pending PR to review:", len(pullRequests))
print()
print("Number of pending PR created this year:",dfPR[dfPR['creationDate']>"2022-01-01"]['branch'].count())
print()
print("Already validated PR waiting to be merged")
print(dfPR[dfPR["approvers"] > 0])
print()
print("Top 20 most solicited reviewers:")
print(dfApprovers.groupby('name').size().sort_values(ascending=False).head(20))
print()
print("Top 20 oldest PR")
print(dfPR.sort_values(by="creationDate", ascending=True).head(20))
print()
print("Average number of reviewer per PR:", round(dfPR['reviewers'].mean(), 2))
print("Ratio of already validated PR", 100*dfPR[dfPR["approvers"] > 0]['branch'].count() / len(pullRequests), '%' )
print("Ratio with at least 2 validations", 100*dfPR[dfPR["approvers"] > 1]['branch'].count() / len(pullRequests), '%' )
print("Mean PR creation date:", dfPR['creationDate'].mean())
print("Median PR creation date:", dfPR['creationDate'].median())
print("Number of involved approvers:", dfApprovers['name'].nunique())
print(dfApprovers.groupby('name').size().sort_values(ascending=False))
print()
print("Branches with merged PR")
print(dfPR[dfPR['close_source_branch']==False])
