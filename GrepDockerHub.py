from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles

##################################################################################################################
##  Parametrized script searching for provided string in a build, a job or a view.                              ##
#   It will return list of builds containing keyword and it will return list of elements words without space    ##
#   containing this keyword.                                                                                    ##
#   Sample:                                                                                                     ##
#   python GrepDockerHub.py 'http://jenkyriba.kod.kyriba.com/job/trunk_integrate_integrator_pipeline/642/' \    ##
#                           --numDays 10 \                                                                      ##
#                           --searchedString Exception                                                          ##
##################################################################################################################


parser = argparse.ArgumentParser(description="Search for string usage in every underlying Jenkins build.\
                                              By default searched string is 'docker.io' and considered builds are \
                                              from one month only.",
                                 epilog="Sample: python GrepDockerHub.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--searchedString", type=str, nargs='?', help="Searched string", default="docker.io")
parser.add_argument("--consideredBuildStatus", type=str, nargs='?', help="Restrict search to specified build status (SUCCESS/FAILURE...)", default="FAILURE")
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', args.consideredBuildStatus, url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, args.consideredBuildStatus, startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', args.consideredBuildStatus, url, startDate, now)
    builds.extend(urlBuilds)

# For each build search for expected string in log
for build in builds:
    data = commonBuilds.getBuildLog(build['url'])
    index = data.find(args.searchedString)
    result = ""
    if index > 0:
        result = data[index:index+80]
        result = result.split()[0]
        print(build['url'] + ' --> ', result)
    build['tag'] = result

commonBuilds.showProgress (str(len(builds)) + " elements found")

# Display distribution of found string
print ()
if builds:
    df = pd.json_normalize(builds)
    df.rename(columns={'tag':'Distribution:'})
    print (df.groupby('tag').size())
else:
    print ("No element found")