from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import common.plot as plot

##################################################################################################################
##  Parametrized script searching for build status distribution in a Jenkins url.                               ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GetBuildStatusDistribution.py 'http://jenkyriba.kod.kyriba.com/' \                                   ##
#                           --numDays 10 \                                                                      ##
#                           --plot                                                                              ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for build status distribution.",
                                 epilog="Sample: python GrepBuildStatusDistribution.py 'https://jenkins-prod-1.kapp.svc.k8s.kod.kyriba.com/' --numDays 1 --plot")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numHours", type=int, nargs='?', help="Number of considered hours for build search", default=0)
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=0)
parser.add_argument("--plot", help="If specified pie chart will be displayed to represent distribution", action='store_true')
parser.add_argument("--urlFilter", type=str, nargs='?', help="Only consider urls containing provided substring", default="")
parser.add_argument("--showBuilds", help="If specified each considered build will be displayed", action='store_true')
args = parser.parse_args()

now = datetime.datetime.now()
startDate = now
if (args.numDays):
    startDate = startDate + relativedelta(days=-args.numDays)

if (args.numHours):
    startDate = startDate + relativedelta(hours=-args.numHours)
print (startDate)

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', None)

# Retrieve list of considered builds
builds = []
for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)

# Display distribution of found string
print ()
if builds:
    df = pd.json_normalize(builds)
    if args.urlFilter:
        df = df[df['url'].str.contains(args.urlFilter)]

    if args.showBuilds:
        print (df[["url", "name", "result"]])
    tagsDistribution = df.groupby('result').size().sort_values(ascending=True)
    print (tagsDistribution)

    # Plot result if required
    if args.plot:
        plot.displayAsPieChart("BUILD STATUS",
                                tagsDistribution,
                                tagsDistribution.index.values,
                                commonColors.getStatusColors(tagsDistribution.index.values))

#    javaee = df[df['url'].str.startswith("http://jenkyriba.kod.kyriba.com/job/pull-request_javaee_test")]
#    print (javaee[["url", "result"]])
#
#    print ("Number of elements: ", len(javaee))
#
#    print("Step 1: remove build")
#    df['url'] = df['url'].str.replace(r'/\d+/$', "", regex=True)
#    df['url'] = df['url'].str.replace(r'/\d+$', "", regex=True)
#    winners = df.groupby('url').size().sort_values(ascending=False).head(10)
#    print (winners)
#
#    for i in range(3):
#        print("Step ", i + 1, ": remove suffix _")
#        df = df[~df['url'].isin(winners.index.values)]
#        df['url'] = df['url'].str.replace(r'_[a-zA-Z0-9]+$', "", regex=True)
#        winners = df.groupby('url').size().sort_values(ascending=False).head(10)
#        print (winners)

else:
    print ("No element found")