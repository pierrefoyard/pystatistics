from collections import Counter
import datetime
import pandas as pd
from dateutil.relativedelta import *
import argparse
import time
import common.builds as commonBuilds
import common.files as commonfiles
import common.colors as commonColors
import os
import json

##################################################################################################################
##  Parametrized script searching for build status distribution in a Jenkins url.                               ##
#   It will return distribution of failing stages in different builds.                                          ##
#   Sample:                                                                                                     ##
#   python GetBuildStatusDistribution.py 'http://jenkyriba.kod.kyriba.com/' \                                   ##
#                           --numDays 10 \                                                                      ##
#                           --plot                                                                              ##
##################################################################################################################

parser = argparse.ArgumentParser(description="Search for test info in provided path.",
                                 epilog="Sample: python AnalyseTestSummaries.py '/tmp/summaries'")
parser.add_argument("jenkinsUrl", type=str, nargs='+', help="List of Jenkins url, could be a view, a job or a build (for view and job we will search for any underlying build)")
parser.add_argument("--numDays", type=int, nargs='?', help="Number of considered days for build search", default=30)
parser.add_argument("--ignoreTrunk", help="If specified we will ignore url containing trunk", action='store_true')
args = parser.parse_args()

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_rows', None)

now = datetime.datetime.now()
startDate = now + relativedelta(days=-args.numDays)

# Retrieve list of considered builds
builds = []

for url in args.jenkinsUrl:
    # Load from cache if exists
    urlBuilds = commonfiles.loadIfExists('builds', "", url, startDate, now)
    if not urlBuilds:
        urlBuilds = commonBuilds.getBuilds(url, "", startDate)
        commonfiles.saveAsFile(urlBuilds, 'builds', "", url, startDate, now)
    builds.extend(urlBuilds)


def validTest(test):
    if not "name" in test:
        return False
    return test["name"].endswith("ScenarioSuite") or \
        (test["enclosingBlockNames"] and test["enclosingBlockNames"][-1] == "kyapi_core_smoke")


# For each build search for expected string in log
data = []
for build in builds:
    if (not args.ignoreTrunk) or (not 'trunk' in build['url']):
        testMap = {}
        jsonFile = commonBuilds.getUnderlyingTests(build['url'])
        if "suites" in jsonFile:
            for test in jsonFile["suites"]:
                if validTest(test):
                    name = test["enclosingBlockNames"][-1]
                    for case in test["cases"]:
                        if name not in testMap:
                            testMap[name] = case["status"]
                        elif (case["status"] != "PASSED"):
                            testMap[name] = case["status"]
                        if testMap[name] != "PASSED":
                            break

            for name in testMap:
                row = {}
                row["name"] = name
                row["failed"] = 0
                row["status"] = testMap[name]
                row["date"] = build['buildDate']
                row["url"] = build['url']
                if testMap[name] == "FAILED":
                    row["failed"] = 1
                data.append(row)

df = pd.json_normalize(data)
print (df[df["failed"] == 1])
print (df[df["name"] == "reg_scf_export_slot_1"])

print(df.groupby("status").size())
col = {'failed':'Number of failures', 'name':'Execution Count'}
df1 = df.groupby('name').agg({'failed':'sum', 'name': 'count'}).rename(columns=col)
df1["Success Rate"] = round(100 - 100*df1["Number of failures"] / df1["Execution Count"])
print (df1.sort_values(by=['Success Rate'], ascending=False))

